use anyhow::Result;
use clap::{App, Arg};
use log::{info, debug};
use simplelog::{Config as LogConfig, LevelFilter, TermLogger, TerminalMode};
use reqwest;

const VERSION: &'static str = env!("CARGO_PKG_VERSION");

macro_rules! fatal {
    ($($tt:tt)*) => {{
        eprintln!($($tt)*);
        ::std::process::exit(1)
    }}
}

fn build_argument_parser<'a, 'b>() -> App<'a, 'b> {
    App::new("gitignorehub")
        .version(VERSION)
        .arg(Arg::with_name("template_name").required(true).index(1))
        .arg(Arg::with_name("verbose").short("v").long("verbose").multiple(true))
}

fn build_client() -> Result<reqwest::blocking::Client> {
    let mut headers = reqwest::header::HeaderMap::new();
    headers.insert("Accept", "application/vnd.github.v3.raw+json".parse()?);
    headers.insert("User-Agent", format!("gitignorehub-rust-{}", VERSION).parse()?);
    debug!("Client headers: {:?}", headers);
    let builder = reqwest::blocking::ClientBuilder::new().default_headers(headers);
    let client = builder.build()?;
    Ok(client)
}

fn main() {
    let matches = build_argument_parser().get_matches();
    {
        let log_level = match matches.occurrences_of("verbose") {
            0 => LevelFilter::Warn,
            1 => LevelFilter::Info,
            _ => LevelFilter::Debug,
        };
        TermLogger::init(
            log_level,
            LogConfig::default(),
            TerminalMode::Stderr,
        )
        .expect("Error creating application logger");
    }
    debug!("Arguments: {:?}", matches.args);
    let template_name = matches.value_of("template_name").unwrap();
    info!("Template name: {:?}", template_name);
    let client = match build_client() {
        Ok(value) => value,
        Err(error) => fatal!("Error creating HTTP client: {}", error),
    };
    debug!("Client: {:?}", client);
    let url = format!("https://api.github.com/gitignore/templates/{}", template_name);
    let request = client.get(&url);
    debug!("Request: {:?}", request);
    let response = match request.send() {
        Ok(value) => value,
        Err(error) => fatal!("Error sending template request: {}", error),
    };
    debug!("Response: {:?}", response);
    let response = match response.error_for_status() {
        Ok(value) => value,
        Err(error) => fatal!("Error in template request: {}", error),
    };
    let template_contents = match response.text() {
        Ok(value) => value,
        Err(error) => fatal!("Error reading request contents: {}", error),
    };
    print!("{}", template_contents);
}
